fun main() {
    val point1= Point(7.0,2.0)
    println(point1)
    val point2= Point(3.0,5.0)
    println(point2)
    val newpoint1= point1.symmetrical()
    println(newpoint1)
    val newpoint2= point2.symmetrical()
    println(newpoint2)
}

class Point(private val x: Double, private val y: Double) {
    override fun toString(): String {
        return "(${x}, ${y})"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Point) {
            val A = x == other.x
            val B = y == other.y
            return A && B
        }

        return false
    }

    fun symmetrical(): Point {
        return Point(-1 * x, -1 * y)
    }


}